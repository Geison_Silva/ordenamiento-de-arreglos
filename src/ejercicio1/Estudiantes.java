package ejercicio1;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author USUARIO
 */
public class Estudiantes {
  private String nombre;
   private String apellido;
    private String identificacion;
    private int puesto;
    private double nota;
    
    public Estudiantes(String nombre, String apellido, String identificacion, int puesto, double nota){
        setNombre(nombre);
        setApellido(apellido);
        setIdentificacion(identificacion);
        setPuesto(puesto);
        setNota(nota);
        
    
}
    public String imprimir(){
        String mensaje = "";
        mensaje = getNombre()+" "+getApellido()+" "+getNota()+" "+getPuesto();
        return mensaje;
    }
    

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public void setNota(double nota) {
        this.nota = nota;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public int getPuesto() {
        return puesto;
    }

    public double getNota() {
        return nota;
    }
    
           
    
}
